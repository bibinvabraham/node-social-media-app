const router = require("express").Router();
const User = require("../models/User");
const bcrypt = require("bcrypt");
router.get("/", (req, res)=>{
    res.send("From Users")
})
// update user
router.put("/:id", async(req, res)=>{
    if(req.body.userId === req.params.id || req.body.isAdmin){
        if(req.body.password){
            try{
                req.body.password = await bcrypt.hash(req.body.password, 10);
            }catch(err){
                return res.status(500).json(err)
            } 
        }
        try{
            const user = await User.findByIdAndUpdate(req.params.id, {
                $set: req.body
            });
            res.status(200).json("Updated Successfully");
        }catch(err){
            res.status(500).json(err);
        }
    }else{
        res.status(403).json("Its not your account");
    }
});
// delete user
router.delete("/:id", async(req, res)=>{
    if(req.body.userId === req.params.id || req.body.isAdmin){
        try{
            const user = await User.findByIdAndDelete(req.params.id);
            res.status(200).json("Deleted Successfully");
        }catch(err){
            res.status(500).send(err);
        }
    }else{
        res.status(403).json("Its not your account");
    }
});
// get user
router.get("/:id", async(req, res)=>{
    try{
        const user = await User.findById(req.params.id);
        const {password, updatedAt, ...other} = user._doc
        res.status(200).json(other);
    }catch(err){
        res.status(500).json(err);
    }
    
})
// follow user
router.put("/:id/follow", async(req, res)=>{
    try{
        if(req.body.userId !== req.params.id){
            const user = await User.findById(req.params.id);
            const currentUser = await User.findById(req.body.userId);
            if(!user.followers.includes(req.body.userId)){
                await user.updateOne({$push:{followers:req.body.userId}});
                await currentUser.updateOne({$push:{following:req.params.id}});
                console.log(user._id)
                res.status(200).json("Followed")
            }else{
                res.status(403).json("Already following user")
            }
        }else{
            res.status(403).json("Cannot follow same user")
        }
    }catch(err){
        res.status(500).json(err);
    }
})
// unfollow user
router.put("/:id/unfollow", async(req, res)=>{
    try{
        if(req.body.userId !== req.params.id){
            const user = await User.findById(req.params.id);
            const currentUser = await User.findById(req.body.userId);
            if(user.followers.includes(req.body.userId)){
                await user.updateOne({$pull:{followers:req.body.userId}});
                await currentUser.updateOne({$pull:{following:req.params.id}});
                console.log(user._id)
                res.status(200).json("Unfollowed")
            }else{
                res.status(403).json("You are not following the user")
            }
        }else{
            res.status(403).json("Cannot unfollow same user")
        }
    }catch(err){
        res.status(500).json(err);
    }
});

module.exports = router