const express = require("express");
const app = express();
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const helmet = require("helmet");
const morgan = require("morgan");
const userRoutes = require("./routes/users");
const authRoutes = require("./routes/auth");
const postRoutes = require("./routes/posts");

dotenv.config();

mongoose.connect(
    process.env.MONGO_URL,
    {useNewUrlParser:true, useUnifiedTopology:true},
    ()=>{
        console.log("Connected to MongoDB")
    }
);

// middleware
app.use(express.json());
app.use(helmet());
app.use(morgan("common"));

// Routes
app.use("/api/users", userRoutes)
app.use("/api/auth", authRoutes)
app.use("/api/posts", postRoutes)

app.listen(3000, ()=>{
    console.log("Connect at 3000")
});
